/* eslint-disable no-console */
require('dotenv').config();

const mongoose = require('mongoose');
const goldModel = require('./models/gold_model');

mongoose.connect(process.env.MONGODB, { useNewUrlParser: true });

(async () => {
  const [find, count] = await Promise.all([goldModel.find(), goldModel.estimatedDocumentCount()]);
  console.log(find);
  console.log(count);
})();
