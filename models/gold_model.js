const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  date: {
    type: String,
    default: null
  },
  currency: {
    type: String,
    default: null
  },
  price: {
    buy: {
      type: Number,
      default: null
    },
    sell: {
      type: Number,
      default: null
    }
  }
});

module.exports = mongoose.model('golds', schema);
