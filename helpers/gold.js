require('dotenv').config();

// eslint-disable-next-line no-underscore-dangle
const _axios = require('axios');
const moment = require('moment-timezone');

const axios = _axios.create();
axios.defaults.timeout = process.env.REQUEST_TIMEOUT;

async function goldBuy() {
  try {
    const response = await axios.get('https://www.indogold.com/ajax/chart_interval/GOLD/0');
    // console.log(response);
    if (response.status === 200) {
      if (response.data.length !== 0) {
        return response.data[0].data[0];
      }
      return null;
    }
    return null;
  } catch (error) {
    throw error;
  }
}

async function goldSell() {
  try {
    const response = await axios.get('https://www.indogold.com/ajax/chart_interval_jual/GOLD/0');
    // console.log(response);
    if (response.status === 200) {
      if (response.data.length !== 0) {
        return response.data[0].data[0];
      }
      return null;
    }
    return null;
  } catch (error) {
    throw error;
  }
}

async function gold() {
  try {
    const date = moment.tz('Asia/Jakarta').format();
    const currency = 'IDR';
    const [buy, sell] = await Promise.all([goldBuy(), goldSell()]);
    if (!buy && !sell) {
      return {
        date
      };
    }
    return {
      date,
      currency,
      price: {
        buy: buy[1],
        sell: sell[1]
      }
    };
  } catch (error) {
    throw error;
  }
}

module.exports = {
  gold,
  goldBuy,
  goldSell
};
