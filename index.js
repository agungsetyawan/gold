/* eslint-disable no-console */
require('dotenv').config();

const mongoose = require('mongoose');
const { CronJob } = require('cron');
const { gold } = require('./helpers/gold');
const GoldModel = require('./models/gold_model');

mongoose.connect(process.env.MONGODB, { useNewUrlParser: true });

async function main() {
  const data = await gold();
  const goldData = new GoldModel(data);
  goldData.save().then(() => console.log(new Date(), 'gold saved'));
}

const job = new CronJob('0 * * * * *', () => {
  main();
});
job.start();
